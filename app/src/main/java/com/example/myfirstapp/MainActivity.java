package com.example.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button1);
        button.setText("Next");

        button.setOnClickListener(this);

        createWeatherMessage(77, "New York");
    }

    private String createWeatherMessage(int temparature, String cityName){
        return "Welcome to " + cityName + " where the temprature is " + temparature +"'F";
    }

    @Override
    public void onClick(View view) {
        button.setText("hasil klik");
    }
}
